import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import javax.xml.stream.events.StartDocument;
import static org.junit
import org.junit.Test;

/**
 * All the tests.
 */
public class Tests {
	
	Airport dummyAirport = new Airport(0, 0, 0, 0);
	Aircraft dummyAircraft = new AirLiner("Spitfire", 33, 443, 4, true, 2, 0, 50, this.dummyAirport);

	/*
	 * Test the ability of setHeading() to deal with 0<angles>360
	 */
	//@Test
	public void testGetAndSetHeading() {
		this.dummyAircraft.setHeading(224);
		assertEquals("set and get aircraft heading", 224, this.dummyAircraft.getHeading());
		
		this.dummyAircraft.setHeading(905);
		assertEquals("set and get aircraft heading at an unusual angle", 185, this.dummyAircraft.getHeading());
	}	
	
	/*
	 * Test turning aircraft at crazy angles
	 */
	@Test
	public void testTurn() {
		this.dummyAircraft.setHeading(45);
		this.dummyAircraft.turn(44);
		assertEquals("aircraft turn(< 360)", 89, this.dummyAircraft.getHeading());
		
		this.dummyAircraft.setHeading(0);
		this.dummyAircraft.turn(456);
		assertEquals("aircraft turn(> 360)", 96, this.dummyAircraft.getHeading());
	
		this.dummyAircraft.setHeading(0);
		this.dummyAircraft.turn(-456);
		assertEquals("aircraft turn(< -360)", 264, this.dummyAircraft.getHeading());
	}
	
	
	/*
	 * Test the results of aircraft velocity component calculations, which involve the use of trigonometry
	 */
	@Test
	public void testVelocityComponents() {
		
		int magnitude = this.dummyAircraft.getVelocity();
		assertEquals(magnitude, 2);
		
		this.dummyAircraft.setHeading(0);
		assertEquals("heading at 0, y component", magnitude, this.dummyAircraft.getVelocityY(), 1);
		assertEquals("heading at 0, x component", 0, this.dummyAircraft.getVelocityX(), 1);
		
		this.dummyAircraft.setHeading(30);
		assertEquals("heading at 30, Y component", 1.73205081, this.dummyAircraft.getVelocityY(), 0.0001);
		assertEquals("heading at 30, X component", 0.99999999, this.dummyAircraft.getVelocityX(), 0.0001);
		
		this.dummyAircraft.setHeading(90);
		assertEquals("heading at 90, Y component", 0, this.dummyAircraft.getVelocityY(), 1);
		assertEquals("heading at 90, X component", magnitude, this.dummyAircraft.getVelocityX(), 1);
	
		this.dummyAircraft.setHeading(163);
		assertEquals("heading at 163, Y component", -1.9126095, this.dummyAircraft.getVelocityY(), 0.0001);
		assertEquals("heading at 163, X component", 0.5847434, this.dummyAircraft.getVelocityX(), 0.0001);

		this.dummyAircraft.setHeading(180);
		assertEquals("heading at 180, Y component", -magnitude, this.dummyAircraft.getVelocityY(), 1);
		assertEquals("heading at 180, X component", 0, this.dummyAircraft.getVelocityX(), 1);

		this.dummyAircraft.setHeading(200);
		assertEquals("heading at 200, Y component", -1.879385, this.dummyAircraft.getVelocityY(), 0.0001);
		assertEquals("heading at 200, X component", -0.684040, this.dummyAircraft.getVelocityX(), 0.0001);
		
		this.dummyAircraft.setHeading(270);
		assertEquals("heading at 270, Y component", 0, this.dummyAircraft.getVelocityY(), 1);
		assertEquals("heading at 270, X component", -magnitude, this.dummyAircraft.getVelocityX(), 1);
		
		this.dummyAircraft.setHeading(350);
		assertEquals("heading at 350, Y component", 1.969615506, this.dummyAircraft.getVelocityY(), 1);
		assertEquals("heading at 350, X component", -0.347296355, this.dummyAircraft.getVelocityX(), 1);
	}
	
}
