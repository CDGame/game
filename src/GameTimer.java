public class GameTimer {
	
	/*
	 * A class which calculates the time in unspecified time units depending on the game frames
	 */
	
	private static int frames = 0;
	private static int msPerFrame = 16;
	
	public static Integer getTime() {		
		return frames * msPerFrame;
	}
	
	public static int getRealTime(){
		return (frames / msPerFrame);
	}
	
	public static void incFrames() {
		frames++;
	}
	
	public static void resetTimer() {
		frames = 0;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
