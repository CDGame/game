public class Waypoint {

	private int longitude;	// Horizontal location
	private int latitude;	// Vertical location
	private int altitude;	// Altitude of waypoint
	private int radius;		// Size of waypoint
	private int UID;		// Number indicating the order the waypoint should be reached, unique within flight plan
	private boolean hit;	// True when the waypoint has been hit by the aircraft
	private int velocity;	// Velocity aircraft should have at the waypoint
	
	
	////////////////////////////////////////
	////								////
	////	Attribute get methods		////
	////								////
	////////////////////////////////////////
	
	public int getLongitude() {
		return longitude;
	}

	public int getLatitude() {
		return latitude;
	}
	
	public int getAltitude() {
		return altitude;
	}
	
	public int getRadius() {
		return radius;
	}
	
	public int getUID() {
		return UID;
	}
	
	public boolean isHit() {
		return hit;
	}
	
	public void hit() {
		hit = true;
	}
	
	
	////////////////////////////////////////
	////								////
	////		Game methods			////
	////								////
	////////////////////////////////////////
	
	
	public Waypoint(int longitude, int latitude, int radius, int UID, int altitude, int velocity){
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.UID = UID;
		this.altitude = altitude;
		this.velocity = velocity;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
