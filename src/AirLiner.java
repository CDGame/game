public class AirLiner extends Aircraft {

	private static int radius = 20;			// aircraft radius in meters
	private static int award = 300; 		// award in points for landing an aircraft
	private static int crew = randomCrew();
	private static int maxAltitude = 45000; //maximum altitude in feet
	private int maxVelocity = 2;
	private static int weight = 50000;		// aircraft's weight in kg
	
	// Constructor
	public AirLiner(String aircraftName, int longitude, int latitude,
			int altitude, boolean flying, int velocity, 
			int minAltitude, int heading, Airport destinationAirport) {
		super(aircraftName, longitude, latitude, altitude, flying, crew,
				weight, velocity, radius, award, maxAltitude, minAltitude, heading, destinationAirport);
	}

	// @return - Randomly generated number of crew members
	private static int randomCrew(){
		return (int) (Math.random()*300+10);
	}
	
}
