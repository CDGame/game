public class Helicopter extends Aircraft {

	private static int radius = 10;			// aircraft radius in meters
	private static int award = 100; 		// award in points for landing an aircraft
	private static int crew = randomCrew();
	private static int maxAltitude = 20000; //maximum altitude in feet
	private int maxVelocity = 2;
	private static int weight = 15000;		// aircraft's weight in kg
	private boolean hovering;
	
	public Helicopter(String aircraftName, int longitude, int latitude,
		int altitude, boolean flying, int velocity,  
		int minAltitude, int heading, Airport destinationAirport) {
	super(aircraftName, longitude, latitude, altitude, flying, crew,
			weight, velocity, radius, award, maxAltitude, minAltitude, heading, destinationAirport);
	}

	public void setHovering(boolean hovering) {
		this.hovering = hovering;
	}
		
	private static int randomCrew(){
		return (int) (Math.random()*10+1);
	}
			
}


