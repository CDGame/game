public class Airport extends Waypoint {

	private int minHeading, maxHeading; // The minimum and maximum headings an aircraft should have when taking off
										// Prevents aircraft flying immediately off-screen
	private static int radius = 30;		// Size of the airport in the airspace
	private static int UID = 0;			// Airport UID is not used by the aircraft
	private static int altitude = 0;	// altitude is always 0
	private static int velocity = 0;	// velocity at witch an airplane needs to land
	
	private Aircraft[] aircraftsPorted;	// an array of aircrafts in the airport
	
	// @return - returns maximum heading for the aircraft taking off
	public int getMaxHeading() {
		return maxHeading;
	}
	
	// @return - returns minimum heading for the aircraft taking off
	public int getMinHeading() {
		return minHeading;
	}

	// @return - returns an array of Aircrafts which are in the airport
	public Aircraft[] getAircraftsPorted() {
		return aircraftsPorted;
	}
	
	// method for putting an aircraft into the array of aircrafts which are in the airport
	public void setAircraftsPorted(Aircraft aircraft, int freeSpace) {
		aircraftsPorted[freeSpace] = aircraft;
	}
	
	// removes all the aircrafts from the airport
	public void resetAircraftsPorted() {
		aircraftsPorted = null;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	//Constructor
	public Airport(int longitude, int latitude, int minHeading, int maxHeading){
		super(latitude, longitude, radius, UID, altitude, velocity);
		this.minHeading = minHeading;
		this.maxHeading = maxHeading;		
	}
}
