public class Score {
	
	/*
	* A class which keeps track of the game score 
	*/
	
	private static int score = 0;
	
	// Returns the current score value
	public static int getScore() {	
		return score;
	}
	
	// Increases the score by the value given in the parameter (negative values accepted)
	public static void increaseScore(int incValue) {
		score += incValue;
	}
	
	public static void scoreReset() {									// Resets the score to zero
		score = 0;
	}
	
	private void addToHighscores() {
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

	
}
