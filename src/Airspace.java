public class Airspace {

	private static int maxAircraft = Game.getMaxAircrafts(); // Maximum number of aircraft allowed in the airspace, method called statically
	private static int typesOfAircraft = 3; // Number of different types of aircrafts
	private static int maxLongitude = 593; 	// Maximum longitude of map (horizontal)
	private static int minLongitude = 0;  	// Minimum longitude of map
	private static int maxLatitude = 570; 	// Maximum latitude of map	(vertical)
	private static int minLatitude = 0;		// Minimum latitude of map
	private static int waypointsPerFlight = Game.getWaypointsPerFlight(); // Number of waypoints per one flight plan, method called statically

	private static boolean paused; // True when the game is paused

	private static int numberOfAircraft = 0; 	// Number of aircraft in airspace
	private static int numberOfAirports = 3; 	// Number of airports in the airspace
	private static Airport[] airports = new Airport[numberOfAirports]; 	// Array of airports
	private static Aircraft[] aircrafts = new Aircraft[maxAircraft]; 	// Array of aircraft

	private static int smallCollisionDistance = 10; // Collision distance for easy mode
	private static int largeCollisionDistance = 30; // Collision distance for hard mode
	private static int collisionWarningDistance = 50; 
	private static int collisionDistance; //Collision distance used (set to either small or large)
	private static int altitudeWarningDistance = 1000;

	////////////////////////////////////////
	////								////
	////	Attribute set/get methods	////
	////								////
	////////////////////////////////////////

	// Returns the maximum latitude of the screen
	public static int getMaxLatitude() {	
		return maxLatitude;
	}

	// Returns the maximum longitude of the screen
	public static int getMaxLongitude() {	
		return maxLongitude;
	}

	// Returns the minimum latitude of the screen
	public static int getMinLatitude() {	
		return minLatitude;
	}

	// Returns the minimum longitude of the screen
	public static int getMinLongitude() {			
		return minLongitude;
	}

	// Returns the maximum number of aircraft allowed in the airspace
	public static int getMaxAircraft() {			
		return maxAircraft;
	}

	// Returns true if the game is paused
	public boolean isPaused() {						
		return paused;
	}

	// Sets pause to true
	public static void pause() {					
		Airspace.paused = true;
	}

	// Un-pauses the game
	public static void start(){						
		Airspace.paused = false;
	}

	// Returns the list of airports in the airspace
	public static Airport[] getAirports() {			
		return airports;
	}

	// Returns the list of aircraft in the airspace
	public static Aircraft[] getAircraft() {		
		return aircrafts;
	}

	// Returns the number of aircraft in the airspace
	public static int getNumberOfAircraft(){		
		return numberOfAircraft;
	}


	////////////////////////////////////
	////							////
	////		Game methods		////
	////							////
	////////////////////////////////////

	// Creates an aircraft and adds it to the aircrafts array
	public static void aircraftGenerator(){					

		// Prevents too many aircraft being generated 
		if(airspaceFull() == false){
			int newAircraftIndex = numberOfAircraft;
			int randomOriginAirport = (int) (Math.random()*numberOfAirports);	 // Selects a random origin airport
			int airportLatitude = airports[randomOriginAirport].getLatitude();	 // Gets the latitude of the selected airport
			int airportLongitude = airports[randomOriginAirport].getLongitude(); // Gets the longitude of the selected airport

			int randomDestinationAirportIndex = (int) (Math.random()*numberOfAirports);	// Selects a random destination airport		
			while (randomDestinationAirportIndex == randomOriginAirport){				// Makes sure that the destination airport is different from the origin
				randomDestinationAirportIndex = (int) (Math.random()*numberOfAirports);	
			}
			Airport destinationAirport = Airspace.getAirports()[randomDestinationAirportIndex];

			if(aircrafts[numberOfAircraft] != null){
				while(aircrafts[newAircraftIndex] != null){
					if(newAircraftIndex == 0){
						newAircraftIndex = maxAircraft;
					}
					else{
						newAircraftIndex -= 1;
					}
				}
			}
			
			
			// Generates a random heading within the range specified by the origin airport
			int randomHeading = (int) (Math.random()*
					(airports[randomOriginAirport].getMaxHeading()-airports[randomOriginAirport].getMinHeading()) + airports[randomOriginAirport].getMinHeading());	

			int randomAircraftType = (int) (Math.random()*(typesOfAircraft - 1)); // Generates a random number indicating a type of aircraft 

			if (randomAircraftType == 0) // Type of aircraft to be generated is Helicopter
			{
				aircrafts[newAircraftIndex] = new Helicopter("Boeing CH-47Chinook", airportLatitude, airportLongitude, 0, true, 2, 0, randomHeading, destinationAirport);
				numberOfAircraft++;
			}
			else if (randomAircraftType == 1) // Type of aircraft to be generated is Airliner
			{
				aircrafts[newAircraftIndex] =  new AirLiner("Boeing 747", airportLatitude, airportLongitude, 0, true, 3, 0, randomHeading, destinationAirport);
				numberOfAircraft++;
			}
			else // Type of aircraft to be generated is LightAircraft
			{
				aircrafts[newAircraftIndex] = new LightAircraft("Cessna 172", airportLatitude, airportLongitude, 0, true, 2, 0, randomHeading, destinationAirport);
				numberOfAircraft++;
			}
		}
	}

	// Removes an aircraft from the airspace
	// @param aircraftIndex	- the index of the aircraft in the aircrafts' array
	public void landAircraft(int aircraftIndex){
		Aircraft landingAircraft = aircrafts[aircraftIndex];
		landingAircraft.land();
		Score.increaseScore(landingAircraft.getAward());
		aircrafts[aircraftIndex] = null;
		// TODO remove waypoints
	}

	// Determines whether the airspace is full
	// @return - true if the number of aircrafts = maxAircraft, false otherwise
	private static boolean airspaceFull(){
		for(int i = 0; i < maxAircraft; i++){
			if(aircrafts[i] == null){
				return false;
			}
		}
		return true;
	}


	// calls violation() or approachingViolation() for any aircrafts which are (approaching) where they shouldn't be
	// @see violation()
	// @see approachingViolation()
	public static void calculateCollisions(){			
		try{
			
			int startingIndex = 0;
			while (aircrafts[startingIndex] == null && startingIndex < maxAircraft - 1)
				startingIndex += 1;
			
			for (int aircraft1Index = startingIndex; aircraft1Index <= maxAircraft; aircraft1Index++){
								
				Aircraft aircraft1 = aircrafts[aircraft1Index];
				
				float aircraft1Latitude = aircraft1.getLatitude();
				float aircraft1Longitude = aircraft1.getLongitude();
				int aircraft1Radius = aircraft1.getRadius();

				// Co-ordinates of the points that make up the square around the centre point of aircraft1
				float aircraft1MinLatitude = aircraft1Latitude - aircraft1Radius/2;
				float aircraft1MaxLatitude = aircraft1Latitude + aircraft1Radius/2;
				float aircraft1MinLongitude = aircraft1Longitude - aircraft1Radius/2;
				float aircraft1MaxLongitude = aircraft1Longitude + aircraft1Radius/2;

				// Destination airport variables
				Airport destinationAirport = aircraft1.getDestinationAirport();
				float airportLongitude = destinationAirport.getLongitude();
				float airportLatitude = destinationAirport.getLatitude();
				int airportRadius = destinationAirport.getRadius();

				// Co-ordinates of the points that make up the square around the centre point of destination airport
				float airportMinLatitude = airportLatitude - airportRadius/2;
				float airportMaxLatitude = airportLatitude + airportRadius/2;
				float airportMinLongitude = airportLongitude - airportRadius/2;
				float airportMaxLongitude = airportLongitude + airportRadius/2;


				if(((airportMinLatitude >= aircraft1MinLatitude && airportMinLatitude <= aircraft1MaxLatitude) ||		
						(airportMaxLatitude >= aircraft1MinLatitude && airportMaxLatitude <= aircraft1MaxLatitude)) 
						&& ((airportMinLongitude >= aircraft1MinLongitude && airportMinLongitude <= aircraft1MaxLongitude) ||
								(airportMaxLongitude >= aircraft1MinLongitude && airportMaxLongitude <= aircraft1MaxLongitude))
						){
					removeAircraft(aircraft1Index);
					return;
				}


				// Checks for violations with the edges of the airspace
				if (minLatitude >= aircraft1Latitude || aircraft1Latitude >= maxLatitude 
						|| minLongitude >= aircraft1Longitude || aircraft1Longitude >= maxLongitude){
					removeAircraft(aircraft1Index);
					return;
				}
				if (minLatitude <= aircraft1Latitude + collisionWarningDistance || aircraft1Latitude + collisionWarningDistance >= maxLatitude 
						|| minLongitude <= aircraft1Longitude + collisionWarningDistance || aircraft1Longitude + collisionWarningDistance >= maxLongitude){

					approachingViolation(aircraft1);
				}

				for (int aircraft2Index = startingIndex; aircraft2Index < maxAircraft; aircraft2Index++){						// Checks for violations with other aircraft
					Aircraft aircraft2 = aircrafts[aircraft2Index];
					if(aircraft1Index == aircraft2Index){}
					else if(aircrafts[aircraft2Index] == null){}
					else if(aircraft2.getAltitude()+altitudeWarningDistance <= aircraft1.getAltitude() 
							|| aircraft2.getAltitude()-altitudeWarningDistance >= aircraft1.getAltitude()){}
					else{
						float aircraft2Latitude = aircraft2.getLatitude();
						float aircraft2Longitude = aircraft2.getLongitude();
						int aircraft2Radius = aircraft2.getRadius();

						// Co-ordinates of the points that make up the square around the centre point of aircraft2
						float aircraft2MinLatitude = aircraft2Latitude - aircraft2Radius/2;
						float aircraft2MaxLatitude = aircraft2Latitude + aircraft2Radius/2;
						float aircraft2MinLongitude = aircraft2Longitude - aircraft2Radius/2;
						float aircraft2MaxLongitude = aircraft2Longitude + aircraft2Radius/2;


						if(((aircraft2MinLatitude >= aircraft1MinLatitude-collisionDistance && aircraft2MinLatitude <= aircraft1MaxLatitude+collisionDistance) ||		
								(aircraft2MaxLatitude >= aircraft1MinLatitude-collisionDistance && aircraft2MaxLatitude <= aircraft1MaxLatitude+collisionDistance)) 
								//If aircraft2's min or max latitude is within aircraft 1's collision range and ...

								&& ((aircraft2MinLongitude >= aircraft1MinLongitude-collisionDistance && aircraft2MinLongitude <= aircraft1MaxLongitude+collisionDistance) ||
										(aircraft2MaxLongitude >= aircraft1MinLongitude-collisionDistance && aircraft2MaxLongitude <= aircraft1MaxLongitude+collisionDistance))
										// ... if aircraft2's min or max longitude is within aircraft 1's collision range
								){
							violation();
							return;
						}

						if(((aircraft2MinLatitude >= aircraft1MinLatitude-collisionWarningDistance && aircraft2MinLatitude <= aircraft1MaxLatitude+collisionWarningDistance) ||		
								(aircraft2MaxLatitude >= aircraft1MinLatitude-collisionWarningDistance && aircraft2MaxLatitude <= aircraft1MaxLatitude+collisionWarningDistance)) 
								//If aircraft2's min or max latitude is within aircraft 1's warning range and ...

								&& ((aircraft2MinLongitude >= aircraft1MinLongitude-collisionWarningDistance && aircraft2MinLongitude <= aircraft1MaxLongitude+collisionWarningDistance) ||
										(aircraft2MaxLongitude >= aircraft1MinLongitude-collisionWarningDistance && aircraft2MaxLongitude <= aircraft1MaxLongitude+collisionWarningDistance))
										// ... if aircraft2's min or max longitude is within aircraft 1's warning range
								){
							approachingViolation(aircraft1);
							approachingViolation(aircraft2);
						}
					}
				}
				try{ // Detects collisions with waypoints in aircraft's course 
					for(int w=0; w < waypointsPerFlight; w++){

						Waypoint waypoint = aircraft1.getCourse()[w];

						if(waypoint.getAltitude() != aircraft1.getAltitude()){
							break;
						}

						int waypointLongitude = waypoint.getLongitude();
						int waypointLatitude = waypoint.getLatitude();
						int waypointRadius = waypoint.getRadius();

						// Co-ordinates of the points that make up the square around the centre point of the waypoint
						int waypointMinLatitude = waypointLatitude - waypointRadius/2;
						int waypointMaxLatitude = waypointLatitude + waypointRadius/2;
						int waypointMinLongitude = waypointLongitude - waypointRadius/2;
						int waypointMaxLongitude = waypointLongitude + waypointRadius/2; 

						if(((waypointMinLatitude >= aircraft1MinLatitude && waypointMinLatitude <= aircraft1MaxLatitude) ||		
								(waypointMaxLatitude >= aircraft1MinLatitude && waypointMaxLatitude <= aircraft1MaxLatitude)) 
								//If aircraft2's minimum or maximum latitude is within the waypoint and ...

								&& ((waypointMinLongitude >= aircraft1MinLongitude && waypointMinLongitude <= aircraft1MaxLongitude) ||
										(waypointMaxLongitude >= aircraft1MinLongitude && waypointMaxLongitude <= aircraft1MaxLongitude))
										// ... if aircraft2's minimum or maximum longitude is within the waypoint
								){
							waypoint.hit();
						}
					}
				}
				catch(NullPointerException ex){} // catch for waypoint NullPointerException

			}// end of main for loop
			return;		
		}
		catch(NullPointerException ex){

		}
		
	}

	/*
	 * Ends the game because of an aircraft violation.
	 * @see Game.gameOver()
	 */
	private static void violation() {
		Game.gameOver();
	}

	// Removes the aircraft from the airspace once it has gone off screen
	private static void removeAircraft(int aircraftIndex){
		numberOfAircraft -= 1;
		Game.setSelectedAircraft(nextAircraft(aircraftIndex));
		Canvas.selectedAircraftLabel(Game.getSelectedAircraft());
		aircrafts[aircraftIndex] = null;
		
		if(aircraftIndex == 0){
			Canvas.hideAircraftLabel(1);
		}
		else{
			Canvas.hideAircraftLabel(2);
		}
		
		aircraftGenerator();
		
		if (numberOfAircraft < 1)
			Game.gameOver();
	}

	/*
	 * Triggers warning that aircraft specified by index is approaching a violation
	 * this method could be used to print the warning on the screen
	 * @param x The index of one of the aircrafts approaching the violation
	 */
	private static void approachingViolation(Aircraft aircraft) {
		
	}

	// Creates preset airports within the airspace
	public static void createAirports() {
		airports[0] = new Airport(100, 100, 0, 90);
		airports[1] = new Airport(500, 500, 180, 270);
		airports[2] = new Airport(400, 5, 45, 180);
	}

	// Sets up the airspace before the game starts
	public static void initialize() {
		createAirports();
		collisionDistance = smallCollisionDistance;
	}


	// Resets the game
	// method could be used for restarting a game
	public static void restart(){
		numberOfAircraft = 0;
		for(Aircraft aircraft : aircrafts){
			try{
				aircraft.setFlying(false);
				aircrafts = new Aircraft[maxAircraft];
			}
			catch(NullPointerException ex){}
		}

		initialize();

	}

	// Takes the index of the currently selected aircraft in the Game and returns the next aircraft in the array
	public static Aircraft nextAircraft(int currentAircraftIndex){
		int index = currentAircraftIndex + 1;
		
		while(aircrafts[index] == null){
			index += 1;
			if (index > (maxAircraft - 1))
				index = 0;
		}
		Aircraft nextAircraft = aircrafts[index];
		Game.setSelectedAircraftIndex(index);
		return nextAircraft;
	}


	// Used by the canvas as the function for the Hard Mode button
	public static void setCollisionDistanceHard(){
		collisionDistance = largeCollisionDistance;
	}

	// Used by the canvas as the function for the Easy Mode button
	public static void setCollisionDistanceEasy(){
		collisionDistance = smallCollisionDistance;
	}


	/**
	 *  * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
