import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Framework that controls the game (Game.java) that created it, update it and draw it on the screen.
 */

public class Framework extends Canvas {
	
    // Width of the frame.
    private static int frameWidth;

    // Height of the frame.
    private static int frameHeight;

    /**
     * Time of one second in nanoseconds.
     * 1 second = 1 000 000 000 nanoseconds
     */
    private static final long secInNanosec = 1000000000L;
    
    /**
     * Time of one millisecond in nanoseconds.
     * 1 millisecond = 1 000 000 nanoseconds
     */
    private static final long milisecInNanosec = 1000000L;
    
    /**
     * FPS - Frames per second
     * How many times per second the game should update?
     */
    private final int GAME_FPS = 16;

    // Pause between updates. It is in nanoseconds.
    private final long GAME_UPDATE_PERIOD = secInNanosec / GAME_FPS;
    
    // Game states
    private static enum GameState{STARTING, VISUALIZING, GAME_CONTENT_LOADING, MAIN_MENU, INSTRUCTIONS, OPTIONS, PLAYING, PAUSED, GAMEOVER, HIGH_SCORES}

    // Current state of the game
    private static GameState gameState;
    
    // Elapsed game time in nanoseconds.
    private long gameTime;
    
    // It is used for calculating elapsed time.
    private long lastTime;
    
    // The actual game
    private Game game;
    
    // Image for menu.
    private BufferedImage menuImg, howToImg, loadingImg;
    
    public Framework ()
    {
        super();
        
        gameState = GameState.VISUALIZING;
        
        //We start game in new thread.
        Thread gameThread = new Thread() {
            @Override
            public void run(){
                GameLoop();
            }
        };
        gameThread.start();	
    }
    
    
   /**
     * Set variables and objects.
     * This method is intended to set the variables and objects for this class
     */
    private void Initialize()
    {
    	setLayout(null);
    }
    
    /**
     * Load files - images, sounds, ...
     * This method is intended to load files for this class
     */
    private void LoadContent()
    {
		try
		{
			URL backgroundImgUrl = this.getClass().getResource("/resources/images/main.png");
			menuImg = ImageIO.read(backgroundImgUrl);
		}
		catch (IOException ex) {
			Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
		}

		try
		{
			URL backgroundImgUrl = this.getClass().getResource("/resources/images/howto.png");
			howToImg = ImageIO.read(backgroundImgUrl);
		}
		catch (IOException ex) {
			Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
		}

		try
		{
			URL backgroundImgUrl = this.getClass().getResource("/resources/images/loading.png");
			loadingImg = ImageIO.read(backgroundImgUrl);
		}
		catch (IOException ex) {
			Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
		}

    }
    
    /**
     * In specific intervals of time (GAME_UPDATE_PERIOD) the game/logic is updated and then the game is drawn on the screen.
     */
    private void GameLoop()
    {
        // These two variables are used in VISUALIZING state of the game. We used them to wait some time so that we get correct frame/window resolution.
        long visualizingTime = 0, lastVisualizingTime = System.nanoTime();
        
        // These variables are used for calculating the time that defines for how long we should put threat to sleep to meet the GAME_FPS.
        long beginTime, timeTaken, timeLeft;
        
        while(true)
        {
            beginTime = System.nanoTime();
            
            switch (gameState)
            {
                case PLAYING:
                	
                    gameTime += System.nanoTime() - lastTime;
                    
                    game.UpdateGame(gameTime, mousePosition());
                    
                    lastTime = System.nanoTime();
                break;
                case PAUSED:
                    //...
                break;
                case GAMEOVER:
                    //...
                break;
                case MAIN_MENU:
                    //...
                break;
                case OPTIONS:
                    //...
                break;
                case HIGH_SCORES:
                    //...
                break;
                case GAME_CONTENT_LOADING:
                    //...
                break;
                case STARTING:
                    // Sets variables and objects.
                    Initialize();
                    // Load files - images, sounds, ...
                    LoadContent();

                    // When all things that are called above finished, we change game status to main menu.
                    gameState = GameState.MAIN_MENU;
                break;
                case VISUALIZING:
                    // We wait one second for the window/frame to be set to its correct size on Ubuntu.
                    // We also insert 'this.getWidth() > 1' condition in case the window/frame size isn't set in time,
                    if(this.getWidth() > 1 && visualizingTime > secInNanosec)
                    {
                        frameWidth = this.getWidth();
                        frameHeight = this.getHeight();

                        // When we get size of frame we change status.
                        gameState = GameState.STARTING;
                    }
                    else
                    {
                        visualizingTime += System.nanoTime() - lastVisualizingTime;
                        lastVisualizingTime = System.nanoTime();
                    }
                break;
            }
            
            // Repaint the screen.
            repaint();
            
            // Here we calculate the time that defines for how long we should put threat to sleep to meet the GAME_FPS.
            timeTaken = System.nanoTime() - beginTime;
            timeLeft = (GAME_UPDATE_PERIOD - timeTaken) / milisecInNanosec; // In milliseconds
            // If the time is less than 10 milliseconds, then we will put thread to sleep for 10 millisecond so that some other thread can do some work.
            if (timeLeft < 10) 
                timeLeft = 10; //set a minimum
            try {
                 //Provides the necessary delay and also yields control so that other thread can do work.
                 Thread.sleep(timeLeft);
            } catch (InterruptedException ex) { }
        }
    }
    
    /**
     * Draw the game to the screen. It is called through repaint() method in GameLoop() method.
     */
    @Override
    public void Draw(Graphics2D g2d)
    {
        switch (gameState)
        {
            case PLAYING:
                game.Draw(g2d, mousePosition());
                GameTimer.incFrames();
            break;
            case PAUSED:
                //...
            break;
            case GAMEOVER:
                game.DrawGameOver(g2d, mousePosition(), gameTime);
            break;
            case MAIN_MENU:
                g2d.drawImage(menuImg, 0, 0, frameWidth, frameHeight, null);
            break;
            case INSTRUCTIONS:
                g2d.drawImage(howToImg, 0, 0, frameWidth, frameHeight, null);
                g2d.setColor(Color.black);
                g2d.drawString("Navigate planes through their waypoints and reach their destination airport", 50, 300);
                g2d.drawString("Waypoints can only be crossed when the plane is at the same location and altitude as the waypoint", 50, 330);
                g2d.drawString("Top number near the waypoint indicates the altitude, the bottom number indicates the order in which it should be reached",50, 360);
                g2d.drawString("The game is over when aircrafts collide", 50, 390);
                g2d.drawString("Press any key to come back to the main menu.", 10, 550);
            break;
            case OPTIONS:
                //...
            break;
            case HIGH_SCORES:
                //...
            break;
            case GAME_CONTENT_LOADING:
                g2d.drawImage(loadingImg, 0, 0, frameWidth, frameHeight, null);
            break;
        }
    }
    
    // method for changing the game state
    public static void setGameState(String state){
    	if(state == "gameover")
    		gameState = GameState.GAMEOVER;
    	else if(state == "loading")
    		gameState = GameState.GAME_CONTENT_LOADING;
    	else if(state == "playing")
    		gameState = GameState.PLAYING;
    }
    
    public static int getFrameWidth(){
    	return frameWidth;
    }

    public static int getFrameHeight(){
    	return frameHeight;
    }

    /**
     * Starts new game.
     */
    private void newGame()
    {
        // We set gameTime to zero and lastTime to current time for later calculations.
        gameTime = 0;
        lastTime = System.nanoTime();
        
        game = new Game();
    }
    
    /**
     *  Restart game - reset game time and call RestartGame() method of game object so that reset some variables.
     */
    private void restartGame()
    {
        // We set gameTime to zero and lastTime to current time for later calculations.
        gameTime = 0;
        lastTime = System.nanoTime();
        GameTimer.resetTimer();
        
        game.RestartGame();
        
        Canvas.hideComponents();
        
        // We create a new game.
        newGame();
    }
    
    /**
     * Returns the position of the mouse pointer in game frame/window.
     * If mouse position is null than this method return 0,0 coordinate.
     * 
     * @return Point of mouse coordinates.
     */
    private Point mousePosition()
    {
        try
        {
            Point mp = this.getMousePosition();
            
            if(mp != null)
                return this.getMousePosition();
            else
                return new Point(0, 0);
        }
        catch (Exception e)
        {
            return new Point(0, 0);
        }
    }
    
    /**
     * This method is called when keyboard key is released.
     * 
     * @param e KeyEvent
     */
    @Override
    public void keyReleasedFramework(KeyEvent e)
    {
        switch (gameState)
        {
            case INSTRUCTIONS:
                gameState = GameState.MAIN_MENU;
            break;
            case GAMEOVER:
            	restartGame();
            break;
        }
    }
    
    /**
     * This method is called when mouse button is clicked.
     * 
     * @param e MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent e)
    {
    	Point mousePosition = mousePosition();
    	if(gameState == GameState.MAIN_MENU){
        	if(mousePosition.getX() >= 96 && mousePosition.getX() <= 347){
        		if(mousePosition.getY() >= 256 && mousePosition.getY() <= 331){
        			newGame();
        		}
        		else if(mousePosition.getY() >= 345 && mousePosition.getY() <= 420){
        			gameState = GameState.INSTRUCTIONS;
        		}
        	}
    	}
    }
}
