import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;

/** 
 * This initialises things, and draws many of the non-button parts of the GUI
 */

public class Game {

	// Game background image.
	private BufferedImage backgroundImg, gameOverImg;

	public static int maxAircraft = 5;				// Maximum number of aircraft allowed in airspace  
	public static int waypointsPerFlight = 3;		// Number of waypoints each flight plan needs

	public static Aircraft selectedAircraft;
	public static int selectedAircraftIndex;

	public Game()
	{
		Framework.setGameState("loading");
		
		Thread threadForInitGame = new Thread() {
			@Override
			public void run(){
				// Sets variables and objects for the game.
				Initialize();
				// Load game files (images, sounds, ...)
				LoadContent();

				Framework.setGameState("playing");
				Canvas.showComponents();

			}
		};
		threadForInitGame.start();
	}


	/**
	 * Set variables and objects for the game.
	 */
	private void Initialize()
	{
		Airspace.initialize(); 
		
	}

	
	public static int getWaypointsPerFlight(){
		return waypointsPerFlight;
	}

	public static int getMaxAircrafts(){
		return maxAircraft;
	}

	/**
	 * Load game files - images, sounds, ...
	 */
	private void LoadContent()
	{
		try
		{
			URL backgroundImgUrl = this.getClass().getResource("/resources/images/grass tile.png");
			backgroundImg = ImageIO.read(backgroundImgUrl);
		}
		catch (IOException ex) {
			Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
		}

		try
		{
			URL backgroundImgUrl = this.getClass().getResource("/resources/images/gameOver.png");
			gameOverImg = ImageIO.read(backgroundImgUrl);
		}
		catch (IOException ex) {
			Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
		}

	}


	/**
	 * Restart game - reset some variables.
	 */
	public void RestartGame()
	{
		Score.scoreReset();
		Airspace.restart();
	}


	/**
	 * Update game logic, including changing aircraft's latitudes and longitudes based on their velocities. 
	 * 
	 * @param gameTime gameTime of the game.
	 * @param mousePosition current mouse position.
	 */
	public void UpdateGame(long gameTime, Point mousePosition){

		if(GameTimer.getRealTime() == 1 && Airspace.getNumberOfAircraft() == 0){
			Airspace.aircraftGenerator();
			selectedAircraft = Airspace.getAircraft()[0];  
			selectedAircraftIndex = 0;
			Canvas.selectedAircraftLabel(selectedAircraft);
		}

		if(GameTimer.getRealTime() == 3 && Airspace.getNumberOfAircraft() == 1){
			Airspace.aircraftGenerator();
		}

		Airspace.calculateCollisions();

		for(Aircraft aircraft : Airspace.getAircraft()) {
			try {

				aircraft.setLatitude((int)(aircraft.getLatitude() + aircraft.getVelocityY()));
				aircraft.setLongitude((int)(aircraft.getLongitude() + aircraft.getVelocityX()));
			}
			catch(NullPointerException ex) {}
		}

		//if(Airspace.getNumberOfAircraft() < 1) {
		//	Framework.setGameState("gameover");
		//}
	}


	/**
	 * Draw the labels, waypoints, aircraft and airports to the screen.
	 * 
	 * @param g2d Graphics2D
	 * @param mousePosition current mouse position.
	 * @param Waypoint 
	 */
	public void Draw(Graphics2D g2d, Point mousePosition)
	{
		Shape controlRectangle = new Rectangle2D.Double(Framework.getFrameWidth()*.75, 0, Framework.getFrameWidth()/4, Framework.getFrameHeight());

		g2d.drawImage(backgroundImg, 0, 0, Framework.getFrameWidth(), Framework.getFrameHeight(), null);

		
		g2d.setColor(Color.white); // Rectangle occupying the right hand quarter of the frame
		g2d.fill(controlRectangle);
		g2d.setColor(Color.black);
		g2d.draw(controlRectangle);

			
		String timeString = String.valueOf(GameTimer.getRealTime()); // Timer showing how long the game has been running for 
		g2d.drawString("Time: " + timeString, (int) (Framework.getFrameWidth()*.75 + 20), 20);
		
		String scoreString = String.valueOf(0); //
		g2d.drawString("Score: " + scoreString + " points", (int) (Framework.getFrameWidth()*.75 + 20), 40);

		for(Aircraft aircraft : Airspace.getAircraft()) {
			
			try{        		
				
				float aircraftUpsideDownLatitude = Framework.getFrameHeight() - aircraft.getLatitude();

				
				if(aircraft == selectedAircraft){ // The selected aircraft is shown in blue, all other aircraft are shown in black
					g2d.setColor(Color.blue);
				}
				else{
					g2d.setColor(Color.black);
				}
				
				// because java2d's (0,0) is at *top* left, and we refuse to conform with that (ours is *bottom* left)
				/*
				g2d.fill(
						new Rectangle2D.Double(	// Generates a rectangle around the centre of the aircraft
								(aircraft.getLongitude()-aircraft.getRadius()/2),
								(aircraftUpsideDownLatitude-aircraft.getRadius()/2),
								aircraft.getRadius(),
								aircraft.getRadius()
								)
						);
				*/
				try {
					InputStream planeStream = getClass().getClassLoader().getResourceAsStream("resources/images/plane1.png");
					Image plane = new ImageIcon(ImageIO.read(planeStream)).getImage();
					plane = rotate(plane, aircraft.getHeading());
					g2d.drawImage(plane, (int) aircraft.getLongitude()-25, (int) aircraftUpsideDownLatitude-20, null, null);
					
					
					g2d.drawString( String.valueOf(aircraft.getAltitude()), (aircraft.getLongitude()+aircraft.getRadius()/2 +1), (aircraftUpsideDownLatitude-aircraft.getRadius()/2));
					
				} catch(IOException ex){}
				for(Waypoint waypoint: aircraft.getCourse()){
					
					float waypointUpsideDownLatitude = Framework.getFrameHeight() - waypoint.getLatitude();
					// because java2d's (0,0) is at *top* left, and we refuse to conform with that (ours is *bottom* left)
					
					if(aircraft == selectedAircraft){ // Waypoints for the selected aircraft are shown first in blue, then green one they've been hit 
						if(waypoint.isHit() == true){	
							g2d.setColor(Color.green);
						}
						else{
							g2d.setColor(Color.blue);
						}
					}
					else{ // Waypoints of other aircraft are shown in black
						g2d.setColor(Color.black);
					}


					g2d.draw(
							new Rectangle2D.Double(	// Generates a rectangle around the centre of the waypoint
									(waypoint.getLongitude() - waypoint.getRadius()/2),
									(waypointUpsideDownLatitude - waypoint.getRadius()/2),
									waypoint.getRadius(),
									waypoint.getRadius()
									)
							);												
					// Labels the waypoint drawn with it's altitude and the order it should be hit in 
					g2d.drawString(String.valueOf(waypoint.getAltitude()), (int) (waypoint.getLongitude() + waypoint.getRadius()/2 + 1), (waypointUpsideDownLatitude - waypoint.getRadius()/2));
					g2d.drawString(String.valueOf(waypoint.getUID() +1), (int) (waypoint.getLongitude() + waypoint.getRadius()/2 + 1), (waypointUpsideDownLatitude - waypoint.getRadius()/2 + 15));
				}
			}
			catch(NullPointerException ex){}
		}


		for(Airport airport: Airspace.getAirports()){ // Generates a rectangle around the centre of the airport
			float airportUpsideDownLatitude = Framework.getFrameHeight() - airport.getLatitude();
			// because java2d's (0,0) is at *top* left, and we refuse to conform with that (ours is *bottom* left)

			Shape airportShape = new Rectangle2D.Double(
					airport.getLongitude() - airport.getRadius()/2,
					airportUpsideDownLatitude - airport.getRadius()/2,
					airport.getRadius(),
					airport.getRadius()
					);
			g2d.setColor(Color.green);
			g2d.fill(airportShape);
			g2d.setColor(Color.black);
			g2d.draw(airportShape);
		}



	}

	
	public static Aircraft getSelectedAircraft(){
		return selectedAircraft;
	}

	public static void setSelectedAircraft(Aircraft aircraft){
		selectedAircraft = aircraft;
	}

	public static void setSelectedAircraftIndex(int index){
		selectedAircraftIndex = index;
	}

	public static void gameOver(){
		Framework.setGameState("gameover");
	}

	
    /**
     * Converts a given Image into a BufferedImage
     *
     * @param img The Image to be converted
     * @return The converted BufferedImage
     */
    public static BufferedImage toBufferedImage(Image img){
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        // Return the buffered image
        return bimage;
    }

    /**
     * Creates an empty image with transparency
     *
     * @param width The width of required image
     * @param height The height of required image
     * @return The created image
     */
    public static Image getEmptyImage(int width, int height){
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        return toImage(img);
    }

    /**
     * Converts a given BufferedImage into an Image
     *
     * @param bimage The BufferedImage to be converted
     * @return The converted Image
     */
    public static Image toImage(BufferedImage bimage){
        // Casting is enough to convert from BufferedImage to Image
        Image img = (Image) bimage;
        return img;
    }
    
    public static Image rotate(Image img, double angle){
        double sin = Math.abs(Math.sin(Math.toRadians(angle))), cos = Math.abs(Math.cos(Math.toRadians(angle)));
        int w = img.getWidth(null), h = img.getHeight(null);
        int neww = (int) Math.floor(w * cos + h * sin), newh = (int) Math.floor(h
                * cos + w * sin);
        BufferedImage bimg = toBufferedImage(getEmptyImage(neww, newh));
        Graphics2D g = bimg.createGraphics();
        g.translate((neww - w) / 2, (newh - h) / 2);
        g.rotate(Math.toRadians(angle), w / 2, h / 2);
        g.drawRenderedImage(toBufferedImage(img), null);
        g.dispose();
        return toImage(bimg);
    }


	/**
	 * Draw the 'game over' screen.
	 * 
	 * @param g2d Graphics2D
	 * @param mousePosition Current mouse position.
	 * @param gameTime Game time in nanoseconds.
	 */
	public void DrawGameOver(Graphics2D g2d, Point mousePosition, long gameTime)
	{
		Canvas.hideComponents();
		Draw(g2d, mousePosition);
        g2d.drawImage(gameOverImg, 0, 0, Framework.getFrameWidth(), Framework.getFrameHeight(), null);
        g2d.setColor(Color.black);
        g2d.drawString("Press any key to restart the game.", Framework.getFrameWidth() / 2 - 100, 500);
	}


}
