public class LightAircraft extends Aircraft {

	private static int radius = 20;			// aircraft radius in meters
	private static int award = 200; 		// award in points for landing an aircraft
	private static int crew = randomCrew();
	private static int maxAltitude = 30000;  //maximum altitude in feet
	private int maxVelocity = 3;				
	private static int weight = 30000;		// aircraft's weight in kg
	
	public LightAircraft(String aircraftName, int longitude, int latitude,
			int altitude, boolean flying, int velocity, int minAltitude, int heading, Airport destinationAirport) {
		super(aircraftName, longitude, latitude, altitude, flying, crew,
				weight, velocity, radius, award, maxAltitude, minAltitude, heading, destinationAirport);
	}

	private static int randomCrew(){
		return (int) (Math.random()*20+5);
	}
	
	
}
