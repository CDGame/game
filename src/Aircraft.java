abstract class Aircraft {
	
	private int numberOfWaypoints = Game.getWaypointsPerFlight();	// The number of waypoints per one flight, the method is called statically
	
	private String aircraftName;	// The name of the aircraft
	private float longitude; 		// Horizontal position of aircraft's centre
	private float latitude;	 		// Vertical position of aircraft's centre
	private int altitude;			// The altitude the aircraft is at
	private Waypoint[] course = new Waypoint[numberOfWaypoints];	// Array of waypoints the aircraft should cross during flight
	private boolean flying;			// False when the aircraft has landed, true when the aircraft is in the air
	private int crew;				// Arbitrarily generated number of crew members
	private int weight;				// Aircraft's weight
	private int velocity;			// speed of the aircraft
	private float velocityX; 		// Number added to the current horizontal location to move aircraft. Calculated from heading and velocity
	private float velocityY;		// Number added to the current vertical location to move aircraft. Calculated from heading and velocity
	private int radius;				// Half the width/length of aircraft.
	private int award;				// Increase in score given for landing the aircraft safely
	private int maxAltitude;		// Maximum altitude during flight (i.e. at a waypoint)
	private int minAltitude; 		// Minimum altitude during flight (i.e. at a waypoint)
	private int heading;			// Direction aircraft is heading in degrees, 0 is downwards
	private Airport destinationAirport; // Airport where aircraft is supposed to land
	
	////////////////////////////////////////
	////								////
	////	Attribute set/get methods	////
	////								////
	////////////////////////////////////////
	
	// @return aircraftName - the name of the aircraft
	public String getAircraftName() {
		return aircraftName;
	}
	
	// @return longitude - aircraft's horizontal position in the airspace
	public float getLongitude() {
		return longitude;
	}
	
	// method for changing aircraft's horizontal position in the airspace
	// @param longitude - new aircraft's horizontal position
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	
	// @return latitude - aircraft's vertical position in the airspace	
	public float getLatitude() {
		return latitude;
	}

	// method for changing aircraft's vertical position in the airspace
	// @param latitude - new aircraft's vertical position
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	// @return altitude - distance from aircraft in the air to the ground
	public int getAltitude() {
		return altitude;
	}

	// method for changing aircraft's altitude in the airspace
	// @param latitude - new aircraft's altitude
	public void setAltitude(int altitude) {
		this.altitude = altitude;
	}
	
	// method for getting an array of waypoints of aircraft's flight plan
	// @return course - an array of waypoints
	public Waypoint[] getCourse() {
		return course;
	}
	
	// @return flying - true if aircraft is in the air, false - aircraft is in the airport
	public boolean isFlying() {
		return flying;
	}
	
	// method used for changing the indication whether the aircraft is flying
	// @param flying - new aircraft's status (true - flying, false - in the airport)
	public void setFlying(boolean flying) {
		this.flying = flying;
	}
	
	// @return crew - returns the number of crew members in the aircraft
	public int getCrew() {
		return crew;
	}
		
	// @return weight - returns the weight of the aircraft
	public int getWeight() {
		return weight;
	}
	
	// @return velocity - returns the speed value the aircraft is at the moment
	public int getVelocity() {
		return velocity;
	}
		
	// @return velocityX - returns the number which is supposed to be added to current aircraft's horizontal position
	public float getVelocityX() {
		return velocityX;
	}
	
	// @return velocityY - returns the number which is supposed to be added to current aircraft's vertical position
	public float getVelocityY() {
		return velocityY;
	}
	
	// @return radius - returns aircraft's radius
	public int getRadius() {
		return radius;
	}
	
	// @return award - returns aircraft's points award
	public int getAward() {
		return award;
	}
	
	// @return maxAltitude - returns maximum aircraft's altitude
	public int getMaxAltitude() {
		return maxAltitude;
	}
	
	// @return minAltitude - returns minimum aircraft's altitude
	public int getMinAltitude() {
		return minAltitude;
	}
	
	/*
	 * Set the heading to the given number of degrees,
	 * and update the velocityX and velocityY.
	 * If heading is outside of 0 and 359, then it will be normalised.
	 * 
	 * @param heading - The new heading, in degrees.
	 */
	public void setHeading(int heading) {
		if (heading >= 360)
			heading = heading%360;
		if (heading < 0){
			heading = Math.abs(heading)%360;
			heading = 360 - heading;
		}
		this.heading = heading;
		updateVelocityComponents();
	}
	
	/*
	 * Changes the aircraft's heading by a given amount.
	 * Values outside of 0 and 359 are changed by setHeading() to the correct range.
	 * 
	 * @uses setHeading()
	 * @param degrees - The angle, in degrees, to turn by
	 */
	public void turn(int degrees){
		setHeading(getHeading() + degrees);
	}

	// @return heading - returns aircraft's current heading
	public int getHeading() {
		return heading;
	}
	
	// @return destinationAirport - returns the Airport object the aircraft is supposed to land at
	public Airport getDestinationAirport() {
		return destinationAirport;
	}

	
	////////////////////////////////////////
	////								////
	////		Game methods			////
	////								////
	////////////////////////////////////////
	

	// Given the aircraft's heading, recalculates the X and Y components of its velocity based on its heading
	private void updateVelocityComponents() { 
		switch (this.getHeading()) {
		case 0:
			velocityX = 0;
			velocityY = getVelocity();
			break;
		case 90: 
			velocityX = getVelocity();
			velocityY = 0;
			break;
		case 180:
			velocityX = 0;
			velocityY = -getVelocity();
			break;
		case 270:
			velocityX = -getVelocity();
			velocityY = 0;
			break;
		default:
			float opp; // vertical movement
			float hyp = getVelocity();
			float adj; // horizontal movement
			float angle = (float) (Math.toRadians(this.getHeading())); // changes the angle to radians so it could calculate sin, cos

			if (this.getHeading() < 90) {
				adj = (float) Math.cos(angle) * hyp; // C=A/H 
				opp = (float) Math.sin(angle) * hyp; // T=O/A
				velocityY = adj; // up
				velocityX = opp; // right
			}
			else if (this.getHeading() < 180) {
				angle -= Math.PI*0.5;
				adj = (float) Math.cos(angle) * hyp;
				opp = (float) Math.sin(angle) * hyp;
				velocityY = -opp; // down
				velocityX = adj; // right
			}
			else if (this.getHeading() < 270) {
				angle -= Math.PI;
				adj = (float) Math.cos(angle) * hyp; 
				opp = (float) Math.sin(angle) * hyp;
				velocityY = -adj; // down
				velocityX = -opp; // left
			}
			else { // aircraft.getHeading() < 360
				angle -= Math.PI*1.5;
				adj = (float) Math.cos(angle) * hyp;
				opp = (float) Math.sin(angle) * hyp;			
				velocityY = opp; // up
				velocityX = -adj; // left
			}
			break;
		}
	}
	
	// method for landing the aircraft, sets altitude to 0
	public void land(){
		setAltitude(0);
		setFlying(false);
	}
	
	//Constructor
	public Aircraft(String aircraftName, int latitude, int longitude,
			int altitude, boolean flying, int crew,
			int weight, int velocity, int radius, int award, int maxAltitude,
			int minAltitude, int heading, Airport destinationAirport) {
		
		this.aircraftName = aircraftName;
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
		this.flying = flying;
		this.crew = crew;
		this.weight = weight;
		this.velocity = velocity;
		this.radius = radius;
		this.award = award;
		this.maxAltitude = maxAltitude;
		this.minAltitude = minAltitude;
		this.heading = heading;
		this.destinationAirport = destinationAirport;
		this.course = createWaypoints();
		updateVelocityComponents();
		Canvas.createAircraftLabel(this); // creates label with aircraft's details, calls method statically
	}

	// creates waypoints for the aircraft's flight plan
	// @return course - returns an array of waypoints
	private Waypoint[] createWaypoints(){
		int waypointAltitude = (int) (Math.random()*(this.maxAltitude - this.minAltitude) + this.minAltitude);	// Calculates a random altitude within the aircraft's range
		
		int averageChangeLatitude = (int) (destinationAirport.getLatitude() - latitude) / (numberOfWaypoints + 2);			// Calculates the average distance between each 
		int averageChangeLongitude = (int) (destinationAirport.getLongitude() - longitude) / (numberOfWaypoints + 2);		
		
		for (int x = 0; x < numberOfWaypoints; x++){
			int waypointLatitude;
			int waypointLongitude;
			
			int baseWaypointLatitude = (int) (latitude + averageChangeLatitude*(x + 1));							// Calculates the original vertical position of the waypoint 
			int randomLatitudeChange = (int) (averageChangeLatitude*Math.random());								// Calculates a random amount of change of no more than the average change 
			
			int randomPositivity = (int) Math.random();															// Randomly decides whether to subtract or add random distance
			if (randomPositivity == 0){
				waypointLatitude = baseWaypointLatitude + randomLatitudeChange;
				if(waypointLatitude >= Airspace.getMaxLatitude()){												// Recalculates the waypoint if it is off-screen (too low)
					waypointLatitude = baseWaypointLatitude - randomLatitudeChange*2;
				}
			}
			else{
				waypointLatitude = baseWaypointLatitude - randomLatitudeChange;
				if(waypointLatitude <= Airspace.getMinLatitude()){												// Recalculates the waypoint if it is off-screen (too high)
					waypointLatitude = baseWaypointLatitude + randomLatitudeChange*2;
				}
			}
			
			
			int baseWaypointLongitude = (int) (longitude + averageChangeLongitude*(x + 1));						// Calculates the original horizontal position of the waypoint 
			int randomLongitudeChange =  (int) (averageChangeLongitude*Math.random());							// Calculates a random amount of change of no more than the average change 
			
			int randomPositivity2 = (int) Math.random();														// Randomly decides whether to subtract or add random distance
			if (randomPositivity2 == 0){
				waypointLongitude = baseWaypointLongitude + randomLongitudeChange;
				if(waypointLongitude >= Airspace.getMaxLongitude()){											// Recalculates the waypoint if it is off-screen (too far right)
					waypointLongitude = baseWaypointLongitude - randomLongitudeChange*2;
				}
			}
			else{
				waypointLongitude = baseWaypointLongitude - randomLongitudeChange;
				if(waypointLongitude <= Airspace.getMinLongitude()){											// Recalculates the waypoint if it is off-screen (too far left)
					waypointLongitude = baseWaypointLongitude + randomLongitudeChange*2;
				}
			}
			Waypoint newWaypoint = new Waypoint(waypointLongitude, waypointLatitude, 10, x, waypointAltitude, velocity);
			course[x] = newWaypoint;																			// Adds the waypoint to the course
			
		}
		return course;
	}

	// Changes the aircraft's altitude from 0 to waypoint height and vice versa
	public void changeAltitude(){
		if(Canvas.headingField.getText() != ""){
			try{
				setAltitude(Integer.parseInt(Canvas.headingField.getText()));
			}
			catch(NumberFormatException ex){}
			
		}
		else{
			setAltitude(course[1].getAltitude());
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
