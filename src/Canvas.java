import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Create a JPanel on which we will draw and listen for keyboard, mouse and action events.
 */

public abstract class Canvas extends JPanel implements KeyListener, MouseListener, ActionListener {

	static JButton changeHeading, turnLeft, turnRight, changeAltitude, changeAircraft, endGame, easyDifficulty, hardDifficulty;
	static TextField headingField;
	static JLabel aircraft1Name, aircraft1Crew, aircraft1Waypoint, aircraft1Waypoint1, aircraft1Waypoint2, aircraft1Waypoint3;
	static JLabel aircraft2Name, aircraft2Crew, aircraft2Waypoint, aircraft2Waypoint1, aircraft2Waypoint2, aircraft2Waypoint3;
	static JLabel selectedAircraftLabel, selectedAircraftLabel1, selectedAircraftLabel2;

	// Values used to facilitate JLabel and JButton placement
	private static int VERTICAL_ALLIGNMENT = 595;
	private static int WIDTH = 180; 
	private static int HEIGHT = 30;
	private static int topHeight = 50;
	private static int heightChange = 35;

	public Canvas()
	{
		// We use double buffer to draw on the screen.
		this.setDoubleBuffered(true);
		this.setFocusable(true);
		this.setBackground(Color.black);

		// Adds the keyboard listener to JPanel to receive key events from this component.
		this.addKeyListener(this);
		// Adds the mouse listener to JPanel to receive mouse events from this component.
		this.addMouseListener(this);

		headingField = new TextField();
		headingField.setVisible(false);
		headingField.setBounds(VERTICAL_ALLIGNMENT, topHeight, WIDTH,HEIGHT);
		this.add(headingField);
		headingField.addMouseListener(this);
		headingField.addActionListener(this);

		changeHeading = new JButton("Change heading");
		changeHeading.setVisible(false);
		changeHeading.setBounds(VERTICAL_ALLIGNMENT, topHeight+30, WIDTH,HEIGHT);
		this.add(changeHeading);
		changeHeading.addActionListener(this);

		turnLeft = new JButton("Turn left");
		turnLeft.setVisible(false);
		turnLeft.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange+30, WIDTH, HEIGHT);
		this.add(turnLeft);
		turnLeft.addActionListener(this);

		turnRight = new JButton("Turn right");
		turnRight.setVisible(false);
		turnRight.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*2+30, WIDTH,HEIGHT);
		this.add(turnRight);
		turnRight.addActionListener(this);

		changeAltitude = new JButton("Change altitude");
		changeAltitude.setVisible(false);
		changeAltitude.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*3+30, WIDTH,HEIGHT);
		this.add(changeAltitude);
		changeAltitude.addActionListener(this);

		changeAircraft = new JButton("Change selected aircraft");
		changeAircraft.setVisible(false);
		changeAircraft.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*4+30, WIDTH,HEIGHT);
		this.add(changeAircraft);
		changeAircraft.addActionListener(this);

		endGame = new JButton("End Game");
		endGame.setVisible(false);
		endGame.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*5+30, WIDTH,HEIGHT);
		this.add(endGame);
		endGame.addActionListener(this);

		easyDifficulty = new JButton("Easy Mode");
		easyDifficulty.setVisible(false);
		easyDifficulty.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*6+30, WIDTH,HEIGHT);
		this.add(easyDifficulty);
		easyDifficulty.addActionListener(this);

		hardDifficulty = new JButton("Hard Mode");
		hardDifficulty.setVisible(false);
		hardDifficulty.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*7+30, WIDTH,HEIGHT);
		this.add(hardDifficulty);
		hardDifficulty.addActionListener(this);

		aircraft1Name = new JLabel();
		aircraft2Name = new JLabel();
		aircraft1Crew = new JLabel();
		aircraft1Waypoint = new JLabel();
		aircraft1Waypoint1 = new JLabel();
		aircraft1Waypoint2 = new JLabel();
		aircraft1Waypoint3 = new JLabel();
		aircraft2Crew = new JLabel();
		aircraft2Waypoint = new JLabel();
		aircraft2Waypoint1 = new JLabel();
		aircraft2Waypoint2 = new JLabel();
		aircraft2Waypoint3 = new JLabel();
		selectedAircraftLabel = new JLabel();
		selectedAircraftLabel1 = new JLabel();
		selectedAircraftLabel2 = new JLabel();
		this.add(aircraft1Name);
		this.add(aircraft1Crew);
		this.add(aircraft1Waypoint);
		this.add(aircraft1Waypoint1);
		this.add(aircraft1Waypoint2);
		this.add(aircraft1Waypoint3);
		this.add(aircraft2Name);
		this.add(aircraft2Crew);
		this.add(aircraft2Waypoint);
		this.add(aircraft2Waypoint1);
		this.add(aircraft2Waypoint2);
		this.add(aircraft2Waypoint3);
		this.add(selectedAircraftLabel);
		this.add(selectedAircraftLabel1);
		this.add(selectedAircraftLabel2);
	}


	// This method is overridden in Framework.java and is used for drawing to the screen.
	public abstract void Draw(Graphics2D g2d);

	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;        
		super.paintComponent(g2d);        
		Draw(g2d);
	}

	/*
	 * Makes the buttons and labels visible
	 */
	public static void showComponents(){
		changeHeading.setVisible(true);
		turnLeft.setVisible(true);
		turnRight.setVisible(true);
		changeAltitude.setVisible(true);
		headingField.setVisible(true);
		changeAircraft.setVisible(true);
		endGame.setVisible(true);
		easyDifficulty.setVisible(true);
		hardDifficulty.setVisible(true);
		aircraft1Name.setVisible(true);
		aircraft1Crew.setVisible(true);
		aircraft1Waypoint.setVisible(true);
		aircraft1Waypoint1.setVisible(true);
		aircraft1Waypoint2.setVisible(true);
		aircraft1Waypoint3.setVisible(true);
		aircraft2Name.setVisible(true);
		aircraft2Crew.setVisible(true);
		aircraft2Waypoint.setVisible(true);
		aircraft2Waypoint1.setVisible(true);
		aircraft2Waypoint2.setVisible(true);
		aircraft2Waypoint3.setVisible(true);
		selectedAircraftLabel.setVisible(true);
		selectedAircraftLabel1.setVisible(true);
		selectedAircraftLabel2.setVisible(true);
	}

	/*
	 * Hides the buttons and labels
	 */
	public static void hideComponents(){
		changeHeading.setVisible(false);
		turnLeft.setVisible(false);
		turnRight.setVisible(false);
		changeAltitude.setVisible(false);
		headingField.setVisible(false);
		changeAircraft.setVisible(false);
		endGame.setVisible(false);
		easyDifficulty.setVisible(false);
		hardDifficulty.setVisible(false);
		aircraft1Name.setVisible(false);
		aircraft1Crew.setVisible(false);
		aircraft1Waypoint.setVisible(false);
		aircraft1Waypoint1.setVisible(false);
		aircraft1Waypoint2.setVisible(false);
		aircraft1Waypoint3.setVisible(false);
		aircraft2Name.setVisible(false);
		aircraft2Crew.setVisible(false);
		aircraft2Waypoint.setVisible(false);
		aircraft2Waypoint1.setVisible(false);
		aircraft2Waypoint2.setVisible(false);
		aircraft2Waypoint3.setVisible(false);
		selectedAircraftLabel.setVisible(false);
		selectedAircraftLabel1.setVisible(false);
		selectedAircraftLabel2.setVisible(false);
	}

	public static void hideAircraftLabel(int label){
		if(label == 1){
			aircraft1Name.setVisible(false);
			aircraft1Crew.setVisible(false);
			aircraft1Waypoint.setVisible(false);
			aircraft1Waypoint1.setVisible(false);
			aircraft1Waypoint2.setVisible(false);
			aircraft1Waypoint3.setVisible(false);
		}
		else{
			aircraft2Name.setVisible(false);
			aircraft2Crew.setVisible(false);
			aircraft2Waypoint.setVisible(false);
			aircraft2Waypoint1.setVisible(false);
			aircraft2Waypoint2.setVisible(false);
			aircraft2Waypoint3.setVisible(false);
		}
	}

	public static void createAircraftLabel(Aircraft aircraft){
		selectedAircraftLabel.setText("Selected Aircraft: ");
		selectedAircraftLabel.setVisible(true);
		selectedAircraftLabel.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+180, WIDTH,HEIGHT/2);

		if(Airspace.getNumberOfAircraft() == 0){
			aircraft1Name.setText(aircraft.getAircraftName());
			aircraft1Name.setVisible(true);
			aircraft1Name.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+30, WIDTH,HEIGHT/2);

			aircraft1Crew.setText("Crew: "+aircraft.getCrew());
			aircraft1Crew.setVisible(true);
			aircraft1Crew.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+40, WIDTH,HEIGHT/2);

			aircraft1Waypoint.setText("Waypoints:");
			aircraft1Waypoint.setVisible(true);
			aircraft1Waypoint.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+50, WIDTH,HEIGHT/2);

			Waypoint waypoint1 = aircraft.getCourse()[0];
			aircraft1Waypoint1.setText("Waypoint 1: ("+waypoint1.getLongitude()+" ,"+waypoint1.getLatitude()+"), "+aircraft.getVelocity()*150+"mph");
			aircraft1Waypoint1.setVisible(true);
			aircraft1Waypoint1.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+60, WIDTH,HEIGHT/2);

			Waypoint waypoint2 = aircraft.getCourse()[1];
			aircraft1Waypoint2.setText("Waypoint 2: ("+waypoint2.getLongitude()+" ,"+waypoint2.getLatitude()+"), "+aircraft.getVelocity()*150+"mph");
			aircraft1Waypoint2.setVisible(true);
			aircraft1Waypoint2.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+70, WIDTH,HEIGHT/2);

			Waypoint waypoint3 = aircraft.getCourse()[2];
			aircraft1Waypoint3.setText("Waypoint 3: ("+waypoint3.getLongitude()+" ,"+waypoint3.getLatitude()+"), "+aircraft.getVelocity()*150+"mph");
			aircraft1Waypoint3.setVisible(true);
			aircraft1Waypoint3.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+80, WIDTH,HEIGHT/2);
		}
		else{
			aircraft2Name.setText(aircraft.getAircraftName());
			aircraft2Name.setVisible(true);
			aircraft2Name.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+100, WIDTH,HEIGHT/2);

			aircraft2Crew.setText("Crew: "+aircraft.getCrew());
			aircraft2Crew.setVisible(true);
			aircraft2Crew.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+110, WIDTH,HEIGHT/2);

			aircraft2Waypoint.setText("Waypoints:");
			aircraft2Waypoint.setVisible(true);
			aircraft2Waypoint.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+120, WIDTH,HEIGHT/2);

			Waypoint waypoint1 = aircraft.getCourse()[0];
			aircraft2Waypoint1.setText("Waypoint 1: ("+waypoint1.getLongitude()+" ,"+waypoint1.getLatitude()+"), "+aircraft.getVelocity()*150+"mph");
			aircraft2Waypoint1.setVisible(true);
			aircraft2Waypoint1.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+130, WIDTH,HEIGHT/2);

			Waypoint waypoint2 = aircraft.getCourse()[1];
			aircraft2Waypoint2.setText("Waypoint 2: ("+waypoint2.getLongitude()+" ,"+waypoint2.getLatitude()+"), "+aircraft.getVelocity()*150+"mph");
			aircraft2Waypoint2.setVisible(true);
			aircraft2Waypoint2.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+140, WIDTH,HEIGHT/2);

			Waypoint waypoint3 = aircraft.getCourse()[2];
			aircraft2Waypoint3.setText("Waypoint 3: ("+waypoint3.getLongitude()+" ,"+waypoint3.getLatitude()+"), "+aircraft.getVelocity()*150+"mph");
			aircraft2Waypoint3.setVisible(true);
			aircraft2Waypoint3.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+150, WIDTH,HEIGHT/2);
		}
	}


	public static void selectedAircraftLabel(Aircraft aircraft){
		if(aircraft == Airspace.getAircraft()[0]){
			selectedAircraftLabel2.setVisible(false);
			selectedAircraftLabel1.setText(aircraft.getAircraftName());
			selectedAircraftLabel1.setVisible(true);
			selectedAircraftLabel1.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+190, WIDTH,HEIGHT/2);
		}
		else{
			selectedAircraftLabel1.setVisible(false);
			selectedAircraftLabel2.setText(aircraft.getAircraftName());
			selectedAircraftLabel2.setVisible(true);
			selectedAircraftLabel2.setBounds(VERTICAL_ALLIGNMENT, topHeight+heightChange*8+190, WIDTH,HEIGHT/2);
		}

	}

	// Methods of the keyboard listener.
	@Override
	public void keyPressed(KeyEvent e) { }

	@Override
	public void keyReleased(KeyEvent e)
	{
		keyReleasedFramework(e);
	}

	@Override
	public void keyTyped(KeyEvent e) { }

	public abstract void keyReleasedFramework(KeyEvent e);


	// Methods of the mouse listener.
	@Override
	public void mousePressed(MouseEvent e){	}

	@Override
	public void mouseReleased(MouseEvent e){ 		 
		if(e.getSource() == headingField) {
			headingField.setText("");			
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) { }

	@Override
	public void mouseEntered(MouseEvent e) { }

	@Override
	public void mouseExited(MouseEvent e) { }

	/*
	 * Actions when buttons are pressed
	 */
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		if(button == changeHeading){
			try{
				int fieldValue = Integer.parseInt(headingField.getText());
				fieldValue = fieldValue%360;
				Game.getSelectedAircraft().setHeading(fieldValue);
			}
			catch(NumberFormatException ex){}
		}
		else if(button == turnLeft){
			Game.getSelectedAircraft().turn(-10);
		}
		else if(button == turnRight){
			Game.getSelectedAircraft().turn(10); 
		}
		else if(button == changeAltitude){
			Game.getSelectedAircraft().changeAltitude();
		}
		else if(button == changeAircraft){
			Game.setSelectedAircraft(Airspace.nextAircraft(Game.selectedAircraftIndex));
			try{
				selectedAircraftLabel(Game.getSelectedAircraft());
			}
			catch(NullPointerException ex){}
		}
		else if(button == endGame){
			Framework.setGameState("gameover");
		}
		else if(button == easyDifficulty){
			Airspace.setCollisionDistanceEasy();
		}
		else if(button == hardDifficulty){
			Airspace.setCollisionDistanceHard();
		}
	}    
}
